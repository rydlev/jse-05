package ru.t1.rydlev.tm.constant;

public final class TerminalConstant {

    public static final String INFO = "info";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String EXIT = "exit";

    public static final String ARGUMENTS = "arguments";

    public static final String COMMANDS = "commands";

}
